package com.example.biin.doan2.Model;

import android.databinding.BaseObservable;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Patterns;

import java.util.regex.Pattern;

public class User extends BaseObservable {

    private int user_id;
    private String user_name;
    private String user_email;
    private String user_password;
    private String user_avatar;
    private String user_addess;
    private String user_phoneNum;
    private String user_createAt;
    private String user_updateAt;

    public User() {
    }

    public User(@NonNull int user_id, @NonNull String user_name, @NonNull String user_email, @NonNull String user_password, String user_avatar, String user_addess, String user_phoneNum, String user_createAt, String user_updateAt) {
        this.user_id = user_id;
        this.user_name = user_name;
        this.user_email = user_email;
        this.user_password = user_password;
        this.user_avatar = user_avatar;
        this.user_addess = user_addess;
        this.user_phoneNum = user_phoneNum;
        this.user_createAt = user_createAt;
        this.user_updateAt = user_updateAt;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public String getUser_addess() {
        return user_addess;
    }

    public void setUser_addess(String user_addess) {
        this.user_addess = user_addess;
    }

    public String getUser_phoneNum() {
        return user_phoneNum;
    }

    public void setUser_phoneNum(String user_phoneNum) {
        this.user_phoneNum = user_phoneNum;
    }

    public String getUser_createAt() {
        return user_createAt;
    }

    public void setUser_createAt(String user_createAt) {
        this.user_createAt = user_createAt;
    }

    public String getUser_updateAt() {
        return user_updateAt;
    }

    public void setUser_updateAt(String user_updateAt) {
        this.user_updateAt = user_updateAt;
    }

    public boolean isValidLoginData() {
        return !TextUtils.isEmpty(getUser_email()) && Patterns.EMAIL_ADDRESS.matcher(getUser_email()).matches() && getUser_password().length() > 6;
    }
}
