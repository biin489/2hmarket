package com.example.biin.doan2.ViewModel;

import android.arch.lifecycle.ViewModel;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.example.biin.doan2.Interface.LoginResultCallbacks;
import com.example.biin.doan2.Model.User;

public class LoginViewModel extends ViewModel {

    private User user;
    private LoginResultCallbacks lrc;

    public LoginViewModel(LoginResultCallbacks lrc) {
        this.lrc = lrc;
        this.user = new User();
    }

    //Phuong thuc lay tai khoan
    public TextWatcher getEmailTextWatcher(){
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                user.setUser_email(s.toString());
            }
        };
    }

    //Phuong thuc lay mat khau
    public TextWatcher getPassTextWatcher(){
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                user.setUser_password(s.toString());
            }
        };
    }

    //Phuong thuc bat su kien login
    public void onLoginClicked(View view){
        if (user.isValidLoginData()){
            lrc.onSuccess("Login success!");
        } else {
            lrc.onError("Login fail!");
        }
    }
}
